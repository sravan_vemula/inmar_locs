from flask import Flask
import json
import mysql.connector

app = Flask(__name__)


'''

CREATE TABLE `InmarDb`.`Locations` 
  `autoid` INT NOT NULL,
  `Location` VARCHAR(45) NOT NULL,
  `Department` VARCHAR(45) NOT NULL,
  `Category` MEDIUMTEXT NULL,
  `SubCategory` MEDIUMTEXT NULL,
  PRIMARY KEY (`autoid`, `Location`, `Department`));


#endpnt 1
SELECT Location from Locations;

#endpnt 2
SELECT * from Locations WHERE Location='Perimeter';

#endpnt 3
SELECT * from Locations where Location='Perimeter' and Department='Bakery';

'''




def create_db_conn(debug=False):
    mydb = None
    try:
        mydb = mysql.connector.connect(
            host="127.0.0.1",
            user="root",
            passwd="",
            database="InmarDb"
        )

        if debug:
            print(mydb)
            print('Connection succesful')
    except Exception as e:
        print(str(e))
        pass
    return mydb

def del_conn(mydb):
    del mydb

@app.route('/verify')
def verify():
    return 'OK'

#/api/v1/location
@app.route('/api/v1/location', methods=['GET'])
def get_all_location():
    db = create_db_conn()
    mycursor = db.cursor(dictionary=True)
    sql = "SELECT * from Locations"
    mycursor.execute(sql)
    results = mycursor.fetchall()
    all_locations = []
    if len(results) > 0:
        for item in results:
            all_locations.append(item)
    tmp_dic = {}
    tmp_dic['Locations'] = all_locations
    return json.dumps(tmp_dic)

# /api/v1/location/{location_id}/department
@app.route('/api/v1/location/<location_id>/department', methods=['GET'])
def get_location_from_loc_id(location_id):
    db = create_db_conn()
    mycursor = db.cursor(dictionary=True)
    sql = "SELECT * from Locations WHERE Location='{}'".format(str(location_id))
    mycursor.execute(sql)
    results = mycursor.fetchall()
    all_locations = []
    if len(results) > 0:
        for item in results:
            all_locations.append(item)
    tmp_dic = {}
    tmp_dic['Locations'] = all_locations
    del_conn(db)
    return json.dumps(tmp_dic)


#http://192.168.0.100:5001/api/v1/location/Center/department/Dairy/category

# /api/v1/location/{location_id}/department/{department_id}/category
@app.route('/api/v1/location/<location_id>/department/<department_id>/category', methods=['GET'])
def get_location_from_locid_dep_id(location_id,department_id):
    db = create_db_conn()
    mycursor = db.cursor(dictionary=True)
    sql = "SELECT * from Locations WHERE Location='{}' and Department='{}' ".format(str(location_id),str(department_id))
    mycursor.execute(sql)
    results = mycursor.fetchall()
    all_locations = []
    if len(results) > 0:
        for item in results:
            all_locations.append(item)
    tmp_dic = {}
    tmp_dic['Locations'] = all_locations
    del_conn(db)
    return json.dumps(tmp_dic)


@app.route('/')
#@auth.login_required
def index():
    return "Hello"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)




